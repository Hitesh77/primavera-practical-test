package com.primavera.example.service;

import java.io.ByteArrayInputStream;

public interface WorkLogService {
	
	boolean isClockedForToday(int userId) throws Exception;

	void clockIn(int userId) throws Exception;

	void clockOut(int userId) throws Exception;
	
	ByteArrayInputStream generateWorklogReport(int userId) throws Exception ;

}
