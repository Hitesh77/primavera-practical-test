package com.primavera.example.service.serviceImpl;

import java.io.ByteArrayInputStream;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.primavera.example.dto.MonthWorkLog;
import com.primavera.example.entity.WorkLog;
import com.primavera.example.repository.WorkLogRepository;
import com.primavera.example.service.WorkLogService;
import com.primavera.example.util.PdfReportUtil;

@Service
public class WorkLogServiceImpl implements WorkLogService {

	@Autowired
	private WorkLogRepository workLogRepository;

	@Autowired
	private PdfReportUtil pdfReportUtil;

	@Override
	public boolean isClockedForToday(int userId) throws Exception {

		return workLogRepository.isClockedForToday(userId) > 0 ? true : false;
	}

	@Override
	public void clockIn(int userId) throws Exception {
		try {
			WorkLog workLog = new WorkLog();
			workLog.setUserId(userId);
			workLog.setClockInDate(new java.sql.Date(new Date().getTime()));
			workLog.setClockInTime(LocalTime.now());
			workLogRepository.save(workLog);

		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void clockOut(int userId) throws Exception {
		try {
			WorkLog workLog = workLogRepository.getUser(userId);
			workLog.setClockOutTime(LocalTime.now());
			workLogRepository.save(workLog);
		} catch (Exception e) {
			throw e;
		}

	}

	@Override
	public ByteArrayInputStream generateWorklogReport(int userId) throws Exception {
		ByteArrayInputStream byteArrayInputStream = null;
		try {
			List<MonthWorkLog> currentMonthWorklogs = workLogRepository.getCurrentMonthWorklogs(userId);
			byteArrayInputStream = pdfReportUtil.generateWorkLogReport(currentMonthWorklogs);

		} catch (Exception e) {
			throw e;
		}
		return byteArrayInputStream;
	}

}
