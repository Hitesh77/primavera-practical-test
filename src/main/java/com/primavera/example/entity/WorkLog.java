package com.primavera.example.entity;

import java.sql.Date;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class WorkLog {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private int userId;

	private LocalTime clockInTime;

	private LocalTime clockOutTime;

	private Date clockInDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public LocalTime getClockInTime() {
		return clockInTime;
	}

	public void setClockInTime(LocalTime clockInTime) {
		this.clockInTime = clockInTime;
	}

	public LocalTime getClockOutTime() {
		return clockOutTime;
	}

	public void setClockOutTime(LocalTime clockOutTime) {
		this.clockOutTime = clockOutTime;
	}

	public Date getClockInDate() {
		return clockInDate;
	}

	public void setClockInDate(Date clockInDate) {
		this.clockInDate = clockInDate;
	}

}
