package com.primavera.example.controller;

import java.io.ByteArrayInputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.primavera.example.entity.User;
import com.primavera.example.service.WorkLogService;

@RestController
public class WorkLogController {

	@Autowired
	private WorkLogService workLogService;

	@RequestMapping(value = { "/dashboard" }, method = RequestMethod.GET)
	public ModelAndView getLoginPage() throws Exception {
		ModelAndView view = new ModelAndView("dashboard");
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) authentication.getPrincipal();
		view.addObject("isClockedForToday", workLogService.isClockedForToday(user.getUserId()));
		return view;
	}

	@RequestMapping(value = "worklogReport", method = RequestMethod.POST, produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> workLogReport() throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) authentication.getPrincipal();
		ByteArrayInputStream inputStream = workLogService.generateWorklogReport(user.getUserId());
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Content-Disposition", "inline; filename=citiesreport.pdf");
		return ResponseEntity.ok().headers(httpHeaders).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(inputStream));

	}

	@RequestMapping(value = "clock-in", method = RequestMethod.POST)
	public ModelAndView clockIn() throws Exception {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) authentication.getPrincipal();
		workLogService.clockIn(user.getUserId());

		return new ModelAndView("redirect:dashboard");
	}

	@RequestMapping(value = "clock-out", method = RequestMethod.POST)
	public ModelAndView clockOut() throws Exception {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) authentication.getPrincipal();
		workLogService.clockOut(user.getUserId());

		return new ModelAndView("redirect:dashboard");
	}
}
