package com.primavera.example.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.primavera.example.dto.MonthWorkLog;

@Component
public class PdfReportUtil {

	public ByteArrayInputStream generateWorkLogReport(List<MonthWorkLog> monthWorkLogs) throws Exception {
		Document document = new Document();

		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);

			Paragraph paragraph = new Paragraph();
			paragraph.add(new Paragraph(" "));
			paragraph.add(new Paragraph("WorkLog Monthly Report", titleFont));
			paragraph.add(new Paragraph(" "));
			paragraph.add(new Paragraph(" "));

			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(60);
			table.setWidths(new int[] { 3, 3 });

			Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Date", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Effective Hours", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			Calendar calendar = Calendar.getInstance();
			int maxDays = calendar.get(Calendar.DAY_OF_MONTH);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
			timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			long differenceInMilliSeconds = 0;

			for (int index = 1; index <= maxDays; index++) {

				String date = dateFormat.format(calendar.getTime());

				PdfPCell cell;
				cell = new PdfPCell(new Phrase(date));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);

				if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
						|| calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
					cell = new PdfPCell(new Phrase("Weekly-Off"));
				} else {
					String effectiveTime = getEffectiveTime(monthWorkLogs, date);
					cell = new PdfPCell(new Phrase(effectiveTime));
					if (!effectiveTime.equals("Absent")) {
						differenceInMilliSeconds += timeFormat.parse(effectiveTime).getTime();
//						
						String[] time = effectiveTime.split(":");
//						differenceInMilliSeconds = differenceInMilliSeconds + Integer.parseInt(time[0].trim()) * 60;
//						differenceInMilliSeconds = differenceInMilliSeconds + Integer.parseInt(time[1].trim());
					}
				}
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell);
				calendar.add(Calendar.DATE, 1);
			}

			PdfPCell cell;
			cell = new PdfPCell(new Phrase("Total Working Hour", headFont));
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new PdfPCell(new Phrase((differenceInMilliSeconds / (60 * 60 * 1000)) + ":" + (differenceInMilliSeconds / (60 * 1000)) % 60 + ":" + (differenceInMilliSeconds / 1000) % 60, headFont));
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);

			PdfWriter.getInstance(document, out);
			document.open();
			document.add(paragraph);
			document.add(table);

			document.close();

		} catch (Exception e) {
			throw e;
		}

		return new ByteArrayInputStream(out.toByteArray());
	}

	public String getEffectiveTime(List<MonthWorkLog> monthWorkLogs, String Date) {
		for (MonthWorkLog monthWorkLog : monthWorkLogs) {
			if (monthWorkLog.getClockInDate().equals(Date)) {
				return monthWorkLog.getClockInTime();
			}
		}
		return "Absent";
	}

}
