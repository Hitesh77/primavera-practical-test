package com.primavera.example.repository;

import org.springframework.data.repository.CrudRepository;

import com.primavera.example.entity.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	User findByUsername(String username);
}
