package com.primavera.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.primavera.example.dto.MonthWorkLog;
import com.primavera.example.entity.WorkLog;

public interface WorkLogRepository extends CrudRepository<WorkLog, Integer> {
	
	@Query("SELECT count(w) FROM WorkLog w where clock_in_date = CURDATE() and clock_out_time IS NULL and user_id =?1")
	long isClockedForToday(int userId);
	
	@Query("SELECT w FROM WorkLog w where clock_in_date = CURDATE() and clock_out_time IS NULL and user_id =?1")
	WorkLog getUser(int userId);
	
	@Query("SELECT new com.primavera.example.dto.MonthWorkLog(clockInDate, (SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(clock_out_time) - TIME_TO_SEC(clock_in_time))) FROM com.primavera.example.entity.WorkLog w1 WHERE w.userId = w1.userId and  w.clockInDate = w1.clockInDate)) FROM WorkLog w where user_id =?1 and clock_out_time is not null and month(clock_in_time) = month(now()) and year(clock_in_time) = year(now()) group by clockInDate")
	List<MonthWorkLog> getCurrentMonthWorklogs(int userId);
	
}
