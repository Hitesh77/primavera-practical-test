package com.primavera.example.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonthWorkLog implements Serializable{

	private static final long serialVersionUID = 1195549943553853055L;
	private final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");


	private String clockInTime;

	private String clockInDate;

	public MonthWorkLog(Date clockInDate, Date clockInTime) {
		super();
		this.clockInTime =timeFormat.format(clockInTime);
		this.clockInDate = dateFormat.format(clockInDate);
	}

	public String getClockInTime() {
		return clockInTime;
	}

	public void setClockInTime(String clockInTime) {
		this.clockInTime = clockInTime;
	}

	public String getClockInDate() {
		return clockInDate;
	}

	public void setClockInDate(String clockInDate) {
		this.clockInDate = clockInDate;
	}
	
	
}
