INSERT INTO `primavera`.`user` (`user_id`, `password`, `status`, `username`) VALUES ('1', '$2a$10$klOiLob1bD4Ohx/tYCQkXuQOcimjZNZudnZ80chSs0qVLpP8/Kq1.', 1, 'admin');

INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('1', '2021-04-01', '09:30:00', '18:00:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('2', '2021-04-02', '10:30:00', '18:00:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('3', '2021-04-05', '09:30:00', '19:00:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('4', '2021-04-06', '09:30:00', '18:30:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('5', '2021-04-07', '11:30:00', '18:23:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('6', '2021-04-08', '09:30:00', '18:10:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('7', '2021-04-09', '09:30:00', '18:00:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('8', '2021-04-12', '09:30:00', '18:00:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('9', '2021-04-14', '09:30:00', '18:23:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('10', '2021-04-15', '10:30:00', '18:00:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('11', '2021-04-16', '09:30:00', '18:23:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('12', '2021-04-19', '10:30:00', '19:00:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('13', '2021-04-20', '09:30:00', '18:00:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('14', '2021-04-21', '10:30:00', '18:23:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('15', '2021-04-22', '09:30:00', '19:00:00', '1');
INSERT INTO `primavera`.`work_log` (`id`, `clock_in_date`, `clock_in_time`, `clock_out_time`, `user_id`) VALUES ('16', '2021-04-23', '10:30:00', '18:00:00', '1');
