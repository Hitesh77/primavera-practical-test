<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Dashboard</title>
</head>
<body>

	<a href="<c:url value="/logout" />">Logout</a>
	<br />
	<br />
	<c:choose>
		<c:when test="${isClockedForToday}">
			<form name='clock-out' action="clock-out" method='POST'>
				<input name="Clock Out" type="submit" value="Clock Out"
					style="color: DodgerBlue;" />
			</form>
		</c:when>
		<c:otherwise>
			<form name='clock-in' action="clock-in" method='POST'>
				<input name="Clock In" type="submit" value="Clock In"
					style="color: DodgerBlue;" />
			</form>
		</c:otherwise>
	</c:choose>


	<br />
	<br />

	<form name='Worklog Report' target="print_popup" action="worklogReport"
		method='POST'>
		<input name="Generate Current month report" type="submit" value="Generate Current month report"
			style="color: DodgerBlue;" />
	</form>

</body>
</html>