<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>login</title>
</head>
<body>
	<h1>Login</h1>
	<c:if test="${param.error != null}">
		<div style="color: red;">You have entered an invalid Username or
			Password.</div>
	</c:if>
	<c:if test="${param.logout != null}">
		<div style="color: green;">You have successfully logged out.</div>
	</c:if>
	<form name='login' action="login" method='POST'>
		<table>
			<tr>
				<td>User:</td>
				<td><input type='text' name='username' value=''></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type='password' name='password' /></td>
			</tr>
			<tr>
				<td><input name="submit" type="submit" value="submit"
					style="color: DodgerBlue;" /></td>
			</tr>
		</table>
	</form>
</body>
</html>